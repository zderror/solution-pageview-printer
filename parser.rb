# frozen_string_literal: true

$LOAD_PATH.unshift "#{File.dirname(__FILE__)}/lib"

require 'page_view'
require 'page_views_reader'
require 'page_view_counts_printer'

reader = PageViewsReader.from_file(ARGV[0])

puts 'List of webpages with most page views ordered from most pages views to less page views: '
PageViewCountsPrinter.new(reader.all).print_to_screen

puts 'list of webpages with most unique page views also ordered: '
PageViewCountsPrinter.new(reader.unique).print_to_screen
