# frozen_string_literal: true

require 'page_views_reader'

describe PageViewsReader do
  context 'given a valid log file' do
    it 'returns all page views' do
      test_log_lines = [
        '/help_page/1 126.318.035.038',
        '/contact 184.123.665.067'
      ]

      page_views = described_class.new(test_log_lines).all

      expect(page_views.count).to eq(2)
    end

    it 'returns unique page views' do
      test_log_lines = [
        '/help_page/1 126.318.035.038',
        '/help_page/1 126.318.035.038',
        '/contact 184.123.665.067'
      ]

      page_views = described_class.new(test_log_lines).unique

      expect(page_views.count).to eq(2)
    end
  end
end
