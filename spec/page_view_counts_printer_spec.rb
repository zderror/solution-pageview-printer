# frozen_string_literal: true

require 'page_view_counts_printer'

describe PageViewCountsPrinter do
  it 'creates a string version of the count report' do
    test_log_lines = [
      '/help_page/1 126.318.035.038',
      '/help_page/1 126.318.035.038',
      '/contact 184.123.665.067'
    ]

    page_views = PageViewsReader.new(test_log_lines).all

    printer = described_class.new(page_views)

    expect(printer.print_to_string).to eq("/help_page/1 2 visits\n/contact 1 visits")
  end

  it 'returns the output sorted by page views in descending order' do
    test_log_lines1 = [
      '/help_page/1 126.318.035.038',
      '/help_page/1 245.000.035.038',
      '/contact 184.123.665.067'
    ]

    test_log_lines2 = [
      '/help_page/1 126.318.035.038',
      '/contact 184.123.665.067',
      '/contact 184.123.665.067'
    ]


    page_views1 = PageViewsReader.new(test_log_lines1).all
    page_views2 = PageViewsReader.new(test_log_lines2).all

    printer1 = described_class.new(page_views1)
    printer2 = described_class.new(page_views2)

    expect(printer1.print_to_string).to eq("/help_page/1 2 visits\n/contact 1 visits")
    expect(printer2.print_to_string).to eq("/contact 2 visits\n/help_page/1 1 visits")
  end
end
