# frozen_string_literal: true

require 'page_view'

describe PageView do
  it 'has a path' do
    page_view = described_class.new('a_path/', '192.168.0.1')

    expect(page_view.path).to eq('a_path/')
  end

  it 'has an ip address' do
    page_view = described_class.new('a_path/', '192.168.0.1')

    expect(page_view.ip_address).to eq('192.168.0.1')
  end

  it 'can be build from a log entry' do
    page_view = described_class.from_log_entry('a_path/ 192.168.0.1')

    expect(page_view.path).to eq('a_path/')
    expect(page_view.ip_address).to eq('192.168.0.1')
  end

  it 'considers two page views with the same path and ip address as equal' do
    page_view = described_class.new('a_path/', '192.168.0.1')
    other_page_view = described_class.new('a_path/', '192.168.0.1')

    expect(page_view).to eql(other_page_view)
  end

  it 'considers page views with the same path and IP non-unique' do
    page_view = described_class.new('a_path/', '192.168.0.1')
    other_page_view = described_class.new('a_path/', '192.168.0.1')

    expect([page_view, other_page_view].uniq.count).to eq(1)
  end
end
