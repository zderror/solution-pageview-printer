# Install
`bundle install`

# Run
`ruby parser.rb webserver.log`

# Test
`rspec`

# Random notes
The code appears to be fairly performant:

|# of lines in the log|execution time|
|---|---|
|500|0.06|
|5000|0.07|
|50000|0.25|
|500000|1.69|
|5000000|17.59|

I did not add any input validation, given the one-off nature of the task.

In real life I would probably use [IPAddr](https://ruby-doc.org/stdlib-2.5.1/libdoc/ipaddr/rdoc/IPAddr.html) to handle the IP addresses, but here it would not work, as the data given contains mostly invalid addresses anyway.

I opted for overwriting `hash` and `eql?` in PageView, in order for `uniq` to work nicely with it (as internally `Array#uniq` converts the data to a hash). I could have instead just call `uniq` on the input file, but that would mean the ability to handle unique page views would need to be implemented separately for each input type. 

You can find code coverage in `coverage/` once you've run `rspec`, I did not test the one line long builder methods (eg. `self.from_file`) or stuff like:

```ruby
def print_to_screen
  puts print_to_string
end
```

and did not go crazy with negative testing, but depending on where we'd normally get the input from and how stable we'd consider it, there is a lot more that could be tested to make the handling of input data a lot more robust.