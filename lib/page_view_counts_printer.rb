# frozen_string_literal: true

class PageViewCountsPrinter
  def initialize(page_views)
    @page_views = page_views
  end

  def print_to_screen
    puts print_to_string
  end

  def print_to_string
    paths_with_counts.map do |path, page_views|
      "#{path} #{page_views.count} visits"
    end.join("\n")
  end

  private

  attr_reader :page_views

  def paths_with_counts
    page_views
      .group_by(&:path)
      .sort_by { |_, page_views| page_views.count }
      .reverse
  end
end
