# frozen_string_literal: true

class PageViewsReader
  def self.from_file(filename)
    new(File.readlines(filename))
  end

  def initialize(log_entries)
    @log_entries = log_entries
  end

  def all
    log_entries.map { |log_entry| PageView.from_log_entry(log_entry) }
  end

  def unique
    all.uniq
  end

  private

  attr_reader :log_entries
end
