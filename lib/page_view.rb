# frozen_string_literal: true

class PageView
  def self.from_log_entry(log_entry)
    path, ip_address = log_entry.split(' ')

    new(path, ip_address)
  end

  attr_reader :path, :ip_address

  def initialize(path, ip_address)
    @path = path
    @ip_address = ip_address
  end

  def eql?(other)
    state == other.state
  end

  def hash
    state.hash
  end

  protected

  def state
    [path, ip_address]
  end
end
